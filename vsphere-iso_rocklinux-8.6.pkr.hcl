source "vsphere-iso" "this" {
  vcenter_server      = var.vsphere_server
  username            = var.vsphere_user
  password            = var.vsphere_password
  datacenter          = var.datacenter
  cluster             = var.cluster
  insecure_connection = true
  http_directory      = "http"
  //convert_to_template = true

  vm_name             = "brtec-RockLinux-8.6"
  guest_os_type       = "centos7_64guest"

  ssh_username        = "packer"
  ssh_password        = "packer"
  ssh_port            = "22"
  ssh_timeout         = "30m"

  CPUs                = 4
  RAM                 = 2048
  RAM_reserve_all     = true

  disk_controller_type =  ["lsilogic-sas"]
  datastore = var.datastore
  storage {
    disk_size = 20000
    disk_thin_provisioned = true
  }

  iso_paths = ["[datastore1] Iso/Rocky-8.6-x86_64-minimal.iso"]
  // iso_checksum = "sha256:b23488689e16cad7a269eb2d3a3bf725d3457ee6b0868e00c8762d3816e25848"

  network_adapters {
    network =  var.network_name
    network_card = "vmxnet3"
  }


  boot_command = [
  "<tab><bs><bs><bs><bs><bs>text ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg<enter><wait>"
  ]
}

build {
  sources  = [
    "source.vsphere-iso.this"
  ]

provisioner "shell-local" {
    inline  = ["echo the address is: $PACKER_HTTP_ADDR and build name is: $PACKER_BUILD_NAME"]
  }
}
